#!/usr/bin/env python3

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

import json
import configparser
import datetime
import pprint
import requests
import string
import time

global_bike_dictionary = {}

def send_message(new_number, message, html_message, config):
    try:
        requests.post(
            config['mail']['link'],
            auth=("api", config['mail']['auth']),
            data={"from": config['mail']['data_from'],
                  "to": config['mail']['data_to'].split(","),
                  "subject": "{} {}".format(config['mail']['data_subject'], new_number),
                  "text": message,
                  "html": html_message})
    except Exception:
        log("Failed to send email api request", "ERROR")

def get_page_dictionary(config, driver):
    dictionary = {}

    try:
        log("Trying to download new webpage")
        driver.get(config['web']['url'])
        bike_list = driver.find_elements_by_xpath(config['web']['filter'])
    except Exception:
        log("Failed to download webpage", "WARNING")
        return False

    log("Full list length: {}".format(len(bike_list)))

    for bike in bike_list:
        # load basic attributes
        bike_object = {
            "data_url": "{}/{}".format(config['web']['url_base_fo'], bike.get_attribute('data-url')),
            "data_category": bike.get_attribute('data-category'),
            "data_series": bike.get_attribute('data-series'),
            "data_id": bike.get_attribute('data-id'),
            "data_wmn": bike.get_attribute('data-wmn'),
            "data_price": bike.get_attribute('data-price'),
            "data_diff": bike.get_attribute('data-diff'),
            "data_date": bike.get_attribute('data-date'),
            "data_size": bike.get_attribute('data-size'),
            "data_state": bike.get_attribute('data-state'),
            "data_year": bike.get_attribute('data-year'),
            "data_top": bike.get_attribute('data-top'),
            "data_pos": bike.get_attribute('data-pos'),
            "data_product": bike.get_attribute('data-product')
        }

        # load json details
        bike_json = json.loads(bike.find_element_by_tag_name('script').get_attribute('innerHTML'))
        bike_object['data_name'] = bike_json['name']
        bike_object['data_image'] = "{}{}".format(config['web']['url_base'], bike_json['image'])
        bike_object['data_sku'] = bike_json['sku']
        bike_object['data_offer_list'] = []

        #for offer in bike_json['offers']:
        offer = bike_json['offers']
        offer_dict = {
            "price": offer['price'],
            "currency": offer['priceCurrency'],
            "condition": offer['itemCondition'],
            "availability": offer['availability']
        }
        bike_object['data_offer_list'].append(offer_dict)

        if bike_object['data_wmn'] == '0' and int(bike_object['data_year']) >= 2018:
            if bike_object['data_id'] in dictionary:
                log("ID {} already in dictionary!".format(bike_object['data_id']), "WARNING")
            else:
                dictionary[bike_object['data_id']] = bike_object

    return dictionary

def get_new_bike_list(config, driver):
    global global_bike_dictionary

    new_bike_listing = {}
    current_bike_dictionary = get_page_dictionary(config, driver)

    if current_bike_dictionary == False:
        return new_bike_listing

    for k in current_bike_dictionary:
        if not k in global_bike_dictionary:
            new_bike_listing[k] = current_bike_dictionary[k]

    log("Downloaded new data: {}, new bikes: {}".format(len(current_bike_dictionary), len(new_bike_listing)))

    global_bike_dictionary = current_bike_dictionary
    return new_bike_listing

def format_bike_data(bike_array):
    body = "<html>"

    for k in bike_array:
        body += "<img src=\"{}\" style=\"width: 100%; heigth: auto;\">".format(bike_array[k]['data_image'])

        body += "<table style=\"border-collapse: collapse; border: 1px solid black;\">"

        body += "<tr>" #name, year
        body += "<td style=\"border: 1px solid black;\">Name</td>"
        body += "<td style=\"border: 1px solid black;\"><a href=\"{}&id={}\">{} {}</a></td>".format(config['web']['url'], bike_array[k]['data_id'], bike_array[k]['data_name'], bike_array[k]['data_year'])
        body += "</tr>"

        body += "<tr>" #price
        body += "<td style=\"border: 1px solid black;\">Price</td>"
        body += "<td style=\"border: 1px solid black;\">€{}(€{})</td>".format(float(bike_array[k]['data_price'])/100, float(bike_array[k]['data_diff'])/100)
        body += "</tr>"

        body += "<tr>" #date
        body += "<td style=\"border: 1px solid black;\">Date</td>"
        body += "<td style=\"border: 1px solid black;\">{}</td>".format(bike_array[k]['data_date'])
        body += "</tr>"

        body += "</table><br><br>"

    body += "</html>"
    return body

def log(message, level = "INFO"):
    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    with open("./canyon.log", "a") as log_file:
        try:
            log_file.write("[{}][{}] {}\n".format(timestamp, level, message))
            log_file.flush()
        except:
            print("[{}][{}] {}\n".format(timestamp, level, message))

## SCRIPT ##

log("Starting new Canyon crawler")
config = configparser.ConfigParser()
try:
    config.read('./config.ini')
except:
    print("Failed to read config file. Exiting!")
    exit()

normal_timeout = int(config['general']['timeout_normal'])

chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument("--disable-gpu")
chrome_options.add_argument("--no-sandbox")

driver = webdriver.Chrome(chrome_options = chrome_options)
driver.implicitly_wait(30)

while True:
    bike_array = get_new_bike_list(config, driver)

    if (len(bike_array) > 0):
        body = format_bike_data(bike_array)
        send_message(len(bike_array), body, body, config)
    else:
        pass

    time.sleep(normal_timeout)
